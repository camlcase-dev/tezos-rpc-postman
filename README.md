# Tezos RPC tutorial and Postman collection 

This tutorial will show developers how to use the Tezos RPC, by providing a Postman collection that developers can download and experiment with. Some pieces, such as the cryptography or account creation, are not possible in Postman alone. For these pieces, code snippets have been provided for developers to run outside of Postman, with instructions on how to copy the information/data back in to the Postman request.





<br/>

## Getting started

In order to run the Postman examples fully you will need 2 Tezos addresses, with funds, revealed and have access to their secret keys to sign payloads. If you have these already, you can skip to the last step [Importing the Postman collection](#4-importing-postman-collection). If you don't continue reading below for step by step instructions.





<br/>

### 1. Setting up the Tezos CLI

- To download and install the Tezos command line client. Follow the "Install" instructions from [tqtezos](https://assets.tqtezos.com/docs/setup/1-tezos-client/).

- Next configure the client to use a publicly available Testnet. 

  _A Testnet is a copy of the Tezos blockchain running independently from the real network, with fake money, but with all the same code/logic/rules/fees etc, to allow developers to experiment without having to pay real money. The primary node we will be using is hosted by [Tezos Giga Node](https://tezos.giganode.io/)._

  To configure the node, run this command in your terminal / command prompt:

  ```bash
  tezos-client -S -A delphinet-tezos.giganode.io -P 443 config update
  ```

- To test its running and we are connected to a Testnet environment, run this command:

  ```bash
  tezos-client rpc get /network/version
  ```

  You should get back something like the below (protocol and timestamp will change over time). If you are connected to a Testnet environment, the chain name be `"TEZOS_<protocol>_<timestamp>"`. If you are on the production system with real money, the chain name will be `"TEZOS_MAINNET"`

  ```javascript
  {"chain_name": "TTEZOS_DELPHINET_2020-09-04T07:08:53","distributed_db_version": 0, "p2p_version": 1}
  ```





<br/>

### 2. Creating two test Tezos addresses

Tezos provides a website where developers can download a file containing an export of a funded Testnet wallet. This is called the Faucet.

<br/>

- Go to the Tezos Testnet [Faucet](https://faucet.tzalpha.net/). Complete the reCAPTCHA and download the file.
- Run this command to import the wallet, and give it a more friendly name of "postman1". Replace `<path-to-file>` with the path to the folder containing the file you just downloaded, and replace `<file-name>` with the name of your file:

  ```bash
  tezos-client activate account postman1 with <path-to-file>/<file-name>.json
  ```

  You should then see a message similar to this (with a different operation hash):

  ```bash
  Node is bootstrapped, ready for injecting operations.
  Operation successfully injected in the node.
  Operation hash is 'opLPsBqXf4ZcV74zBpn6tqjNZymdHJpqUiPV6m9fpxqySSgKFPm'
  Waiting for the operation to be included...
  ```

  Wait for the operation to finish, it will take approx 1 minute for the operation to make its way onto the blockchain. This is true for all operations on Tezos. A new block is created every 60 seconds. Depending on traffic and when you send your operation, it may take longer as you will need to wait for a second or third block.

- Go back to the faucet, download another wallet and repeat this process again, this time naming the wallet `postman2`.
  
  ```bash
  tezos-client activate account postman2 with <path-to-file>/<file-name>.json
  ```

- Test they are setup correctly by sending some XTZ from one to the other, with this command:
  ```bash
  tezos-client transfer 10 from postman1 to postman2 --burn-cap 0.257
  ```

- You now have two local wallets with funds. Take note of their tz1 addresses as you will need them later. Addresses can be accessed with this command:
  
  ```bash
  tezos-client show address postman1 
  ```
  The address you will need is the value called "hash" that starts with "tz1".



<br/>

### 3. Optional: How to create a wallet without using the CLI

There are several third party libraries available to allow developers to create wallets in their own applications. camlCase has 2 libraries available. You can use these directly to build your own applications or use them as a baseline to build your own. These libraries don't offer a way to fund accounts using the faucet directly. So you will need to use the CLI to fund them yourself, or transfer funds from another wallet application.


<br/>

- Native Swift library - [camlKit](https://gitlab.com/camlcase-dev/camlkit)
  
  To create a wallet with a seed phrase (a mnemonic), you can run the below code. More detailed instructions about how to use the library can be be found on its gitlab page.

  ```swift
  Wallet.create(withMnemonic: "word1 word2 word3", writeToKeychain: true) { (wallet) in
	  guard let wallet = wallet else {
		  print("Error creating wallet")
		  return
	  }
			
	  print("Wallet created with address: \(wallet.address)")
  }
  ```


- Native Kotlin library - [KotlinTezos](https://gitlab.com/camlcase-dev/kotlin-tezos)
  
  To create a wallet with a seed phrase (a mnemonic), you can run the below code. More detailed instructions about how to use the library can be be found on its gitlab page.

  ```kotlin
  val listOfWords = listOf("A", "B", "C")
  val wallet = Wallet(listOfWords)

  println("Wallet created with address: " + wallet.address)
  ```

- To fund one of these addresses, use the tezos-client command below to send funds from a funded account replacing:
  - `<amount>` with the amount of XTZ you wish to send, e.g. `10`
  - `<source-address>` with a tz1 address or a saved friendly name, e.g. `postman1`
  - `<destination-address>` with the tz1 address you want to send to, e.g. `tz1RjtZUVeLhADFHDL8UwDZA6vjWWhojpu5w`  
  
  
  ```bash
  tezos-client transfer <amount> from <source-address> to <destination-address> --burn-cap 0.257
  ```

- Once funded, they will also need to `reveal` their publicKey to the Tezos network. Both libraries will handle this automatically behind the scenes when you send your first operation. More details about on how to perform a reveal with the RPC in the tutorials below.





<br/>

### 4. Importing Postman Collection

- Download [Postman](https://www.postman.com/downloads/), if you haven't already.
- Download the contents of this repo by clicking the download button in the top right corner.

  ![Screenshot showing where the download button is](assets/download.png "Download button")

- Unzip the folder.
- Open Postman and click "Import" in the top left corner.

  ![Screenshot showing where the import button is in Postman](assets/import-postman.png "Import button")

- Click "Upload Files" and navigate to the unzipped folder. Inside the folder "Postman-Collection" there is one json file and a folder. Select the JSON file and click import.

- Next Import the pre-configured environments by clicking the settings icon in the top right

  ![Screenshot showing where the settings button is in Postman](assets/postman-environments.png "Settings button")

  Then click import along the bottom and import the `Tezos-Testnet` and `Tezos-Mainnet` environments from the folder "Postman-Environmnets" inside the the same "Postman-Collection" folder.

- Now that the environments are imported, we need to add your newly created wallet addresses to the Testnet environment. Click on the `Tezos-Testnet` environment.

- Find the variables named `sourceAddress` and `destinationAddress` and update their "Initial Value" and "Current Value" to the tz1 addresses we created earlier in the getting started section (or your own if you skipped it). Set `sourceAddress` to the address for `postman1` and `destinationAddress` to the address for `postman2`. It should then look something like this:
  
  ![Screenshot showing what the updated environment should look like](assets/postman-updated-environment.png "Updated environment")

  You can ignore everything else and click update.

- If you have mainnet addresses, you can do the same for `Tezos-Mainnet`. Then you can choose which environment to use for all the API calls by using the drop down, pictured below. You can use the `Tezos-Testnet` to experiment with fake money and the wallets created previously in the "Getting Started" section. Once it's working on Testnet you can switch to `Tezos-Mainnet` and verify your calls work on the real network. Be aware this will cost real money and therefore is not part of this tutorial, just provided for completeness.
  
  ![Screenshot showing where the environment dropdown control is](assets/postman-change-environment.png "Environment dropdown")


- Now you are all set and ready to go, pick a tutorial below to get started.




<br/>
<br/>
<br/>
<br/>
<br/>

## Tutorials

- [Sending an Operation](./tutorial-send-operation.md)
- [Estimate the fees for an Operation](./tutorial-estimate-operation.md)
- [Reveal a public key](./tutorial-reveal-public-key.md)
- Getting the users balance of an FA1.2 Token ... coming soon
- Handling Errors ... coming soon
- Making Dexter calls ... coming soon
- Originating Smart Contracts ... coming soon





<br/>
<br/>
<br/>
<br/>
<br/>

## Useful third party tutorials/sites

- [Tezos StackExchange](https://tezos.stackexchange.com/)
- [Setting up a Tezos Client](https://assets.tqtezos.com/docs/setup/1-tezos-client/)
- [Tezos RPC docs](https://tezos.gitlab.io/api/rpc.html)
- [Signing an Operation](https://www.ocamlpro.com/2018/11/21/an-introduction-to-tezos-rpcs-signing-operations/)
- [Fee calcualtion](https://tezos.gitlab.io/protocols/003_PsddFKi3.html#more-details-on-fees-and-cost-model)





<br/>
<br/>
<br/>
<br/>
<br/>

## Useful third party tools / services

- Public Nodes:
  - [Tezos Giga Node](https://tezos.giganode.io/)
  - [Nautilus Cloud](https://nautilus.cloud/)

<br/>

- Block Explorers and stats:
  - [TZKT.io Block Explorer - Mainnet](https://tzkt.io/)
  - [TZKT.io Block Explorer - Testnet (Delphi protocol)](https://delphi.tzkt.io/)
  - [BakingBad Baker index and stats](https://baking-bad.org/)
  - [Better Call Dev - Contract Explorer](https://better-call.dev/)