# Tutorial: Estimate the fees for an Operation

As more and more advanced applications and tools are being added to Tezos, it's important to add the ability to your apps to estimate the necessary fees automatically. Asking users to choose or enter a fee or gas limits, involves them knowing information about how the blockchain works. This tutorial will cover the process of Forging and estimating the fees needed for each operation. 



<br/>

## Introduction

Its up to the client to tell the Preapply / Inject RPC how much resources this operation will take to process or "Bake". The client must also offer a fee to cover the cost of this processing. Depending on what operation you invoke and to who, you may also need to pay other additional fees. Here is a quick explanation of some terms you will need to know:

- Gas:  
This is a number representing the cost in resources to run the operation. You can think of it like how much CPU and RAM are needed to process and inject the operation.

- Storage:  
How much **addtional** disk space (in bytes) will be needed to complete this operation. E.g. completing the operation may need to add a record of a balance, or store a publicKey. If the operation doesn't require any additional space, but only modifies the existing value, there is no charge.

- Fee:  
The amount of XTZ the user is offering to the network to pay for the cost of processing and injecting. This money will go to the baker that process the block. Bakers will look for a minimum fee before deciding which operations to process. This tutorial will work through calculating this minimum fee. If you had a time-sensitive, valuable trade you wanted to make, offering a higher fee could ensure your operation gets picked up straight away. 

- Burn Fee:  
Not to be confused with the Fee. A burn fee can be thought of as an anti-spam feature. This XTZ does not go to a baker or any third party, it is simply destroyed by the network. Certain operations such as allocating space for a new wallet, incur a burn fee to prevent malicious users from spamming the network to create addresses. The most common burn fee is 0.0064250 XTZ to register a new account.

- Allocation Fee:  
A way to reserve an address is to send XTZ to an address that doesn't already exist. If you do this, you need allocate space for the new address so the funds don't disappear. This is similar to the burn fee, but charged to the send who may not own the new address. Allocation fees are always 0.0064250 XTZ.

- Origination Fee:  
One of the big advantages of the Blockchain, is being able to create and use smart contracts. Once again, creating these contracts (and assigning an address starting with KT1), costs a fee, this is the same charge as the allocation fee, but is incurred for every new contract created.

<br/>

These fees may change in the future. The constants can be found by using the RPC: `/chains/main/blocks/head/context/constants`. Simply multiply `"cost_per_byte"` by the number of bytes you are trying to store to get the value. E.g. It takes 257 bytes to store a public key on the network. 257 * 250 = 64250 Mutez = 0.0064250 XTZ (based on current values).



<br/>
<br/>
<br/>

## Tutorial

<br/>

### Step 1: Refreshing the metadata
First we need to refresh the metadata from the network in Postman. Even if you have already run the below requests, in the previous tutorial, run them again to ensure we have the correct counter. If not, you will get an error back, like the below, indicating that we have an outdated counter:

```javascript
[{"kind":"branch","id":"proto.006-PsCARTHA.contract.counter_in_the_past","contract":"tz1SbqCsj7PA3TXpaziL8nsqWj9xv83zmQ8n","expected":"2725224","found":"2725222"}]
```

<br/>

To start, open the "Sending an Operation" folder and open an send the first 4 requests:

- send: `1_ManagerKey`
- send: `2_Counter`
- send: `3_Head`
- send: `4_Forge`

Now we are sure we have all the data that we need.





<br/>
<br/>
<br/>

### Step 2: Performing a run_operation
The Tezos node has an RPC that will take an Operation and perform a "dry run" in order to simulate the effects of the Operation without injecting it to the network. As part of this, the RPC will calculate the amount of gas each operation consumes and the amount of storage it requires. We can use this RPC and some simple maths to work out what the `fee`, `gas_limit` and `storage_limit` we should be sending to Forge, Preapply etc.

- Open the "Estimating an Operation" folder of the Tezos Postman collection.
- Click and open the only request `1_RunOperation`.
- First thing to note is inside the "Body" tab. We send the same JSON we would send to Preapply, but with 4 key differences:
  - We sent `fee` to zero
  - We set `gas_limit` to the maximum possible value of 800,000
  - We set `storage_limit` to the maximum of 60,000
  - We set `signature` to a default signature (base58check of 128 zeros).

  The reason for the gas, fee and storage changes, is to prevent run_operation from returning any errors connected with insufficient gas or storage. We are using this RPC to calculate what those need to be, so we don't want to waste time guessing first and re-trying.

  The reason for the signature, is that the run_operation call won't check it's the correct signature, but it needs to be a valid value of the right size and format. Using a static default makes it easier and quicker to perform these checks.
- Click send to send the request, you should see something like this:

	```javascript
	{
		"contents": [
			{
				"kind": "transaction",
				"source": "tz1YxUhaLrGr8nbdTGFs2HrJZeUebGMg7cmE",
				"fee": "0",
				"counter": "2312685",
				"gas_limit": "800000",
				"storage_limit": "60000",
				"amount": "1000000",
				"destination": "tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF",
				"metadata": {
					"balance_updates": [],
					"operation_result": {
						"status": "applied",
						"balance_updates": [
							{
								"kind": "contract",
								"contract": "tz1YxUhaLrGr8nbdTGFs2HrJZeUebGMg7cmE",
								"change": "-1000000"
							},
							{
								"kind": "contract",
								"contract": "tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF",
								"change": "1000000"
							}
						],
						"consumed_gas": "10207"
					}
				}
			}
		]
	}
	```
  
  This object now contains all the information we need to calculate the costs. This example is simple, the last key `consumed_gas` is all we are going to use from this response this time. But we'll walk through a generic solution for all operations.




<br/>
<br/>
<br/>

### Step 3: Analysing the results
- First look at the "Test results" tab and you should see a JSON object containing all the figures you need in order to inject this operation. It should look like this:

    ![Screenshot showing the expanded "Sending an Operation" folder](assets/tutorial-estimate-result.png "Sending an Operation")

	If we wanted to really inject this operation, we could now take the `gas_limit`, `storage_limit` and `fee` and add those to the JSON payload sent with Forge and Preapply to ensure this Operation is successful. So how do we calculate these.

<br/>

- Open the "Tests" tab to see a sample Javascript implementation of how to extract and calculate these fees (Note this was only tested with the samples in this Postman collection, and doesn't yet include origination fees. It will be updated when we complete more of the tutorial).

  At the top of the code are some constants. These constants come directly from the Tezos Baker implementation and are defined in "Nano Tez". Nano Tez itself is too small to be sent to the network, so after adding everything up, it needs to be converted to higher scale Mutez.

  In short, for each object in the `contents` array we loop around the Result objects looking for keys `consumed_gas`, `paid_storage_size_diff` and `allocated_destination_contract` (Note, not visible in this example are the `internalOperationResults` which come back when smart contracts call other contracts). We grab all these and add each to a running total. Then for each content object we do this:

  - Add a safety margin to the total gas consumed to ensure no mistakes. Multiply the total gas by the NanoTez constant and convert to mutez (No decimals are allowed).
	- The total gas (with the margin) now becomes our `gas_limit`.
	- The mutez is now half of the `fee` we need.
  - To get the other half of the `fee`, we need to take the forged hex string from the Forge call and add the hex version of our `defaultSignature`. In this case it's 128 zeros in a string. We then count the number of characters and divide by 2 to get the number of bytes. Then we multiply this by the `feePerStorageByte` constant. We then convert this to mutez.
	- Add together the gas mutez and the sotrage mutez to get the final answer for the `fee`.
  - Add together all the `paid_storage_size_diff` and return that as the `storage_limit` (we also add a safety margin just to be safe).
  - Add together all the `paid_storage_size_diff` and multiple by the constant `feePerPaidStorageSizeDiffByte` to get the mutez cost of all the burn fees.
  - Add together all the times `allocated_destination_contract` appeared and multiply that number by the allocation constant `allocationFee`.

  And there we have it, we have now figured out the `fee`, `gas_limit`, `storage_limit`, `burnFee`, and `allocationFee`. The `burnFee` and `allocationFee` don't get used in any of the RPC requests. These are important figures to show to the user before the agree to send an Operation. These are costs that will automatically be deducted from their wallet when the Operation is injected.

<br/>

- Note: The documentation states that when sending multiple Operations together, you need only add the mutez from the forge+signature, to one of the operations. The baker will sum all the fees together and check the total matches the minimum. This will be updated when we add more advanced samples to the tutorial and can prove it works.
