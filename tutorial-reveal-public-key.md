# Tutorial: Revealing your public key to the network

Before a source address is able to send operations or delegate its funds, its public key must be shared with the network so that signed operations can be validated. This is called "Revealing" your public key. Most libraries will handle this automatically behind the scenes when you send your first operation. Here we will take a look at this process.



<br/>

## Introduction
The first tutorial we looked at [Sending an Operation](./tutorial-send-operation.md), used wallet addresses that had already been setup and revealed. This was to avoid added complexity and focus on the steps needed to complete an operation. Now we will take a look at what is required to reveal a wallets public key.

This tutorial will first touch on how to detect whether or not a wallet address needs to be revealed. You must perform a reveal before you can send any operation, but remember users can recovery already revealed wallets, using your applications. If you tried to send a reveal on an account already revealed you will get an error like this:


```javascript
[{"kind":"branch","id":"proto.006-PsCARTHA.contract.previously_revealed_key","contract":"tz1YxUhaLrGr8nbdTGFs2HrJZeUebGMg7cmE"}]
```

Blocking any further actions. Next we will walk through the same set of steps as the first tutorial [Sending an Operation](./tutorial-send-operation.md), to inject a reveal operation to the network.




<br/>
<br/>
<br/>

## Tutorial

<br/>


### Step 1: Create a new unrevealed wallet

Lets use the CLI to create a new, unrevealed wallet. Run the below command to create a new wallet called `postman3`

```bash
tezos-client gen keys postman3
```

and give it some funds so it can pay for the cost of revealing its public key

```bash
tezos-client transfer 10 from postman1 to postman3 --burn-cap 0.257
```

Wait for the operation to complete and then run the below command to see the address and public key of the new wallet.

```bash
tezos-client show address postman3
```

Now:

- Open the folder "Reveal a public key" in the Tezos Postman collection.
- Open the first request `1_ManagerKey`.
- Open the "Pre-request Script" tab.
- Copy the hash from the CLI to replace `<your-new hash-here>` on the first line (this is the tz1 address of the new wallet).
- Copy the public key from the CLI to replace `<your-public-key-here>` on the second line.
- Click Send





<br/>
<br/>
<br/>

### Step 2: Detecting when a reveal is needed
You will notice the response from the first request is `null`. This wallet has no `managerKey`. This means the account has not been revealed. There is no public key that the network can use to validate your signed transactions.

As an example of how libraries handle this, we can see in our Swift library [camlKit](https://gitlab.com/camlcase-dev/camlkit/-/blob/master/Source/Factories/OperationFactory.swift#L72) we can see that there is an `OperationFactory` class with a single method to create `OperationPayload` objects. This method takes in an array of operations and a metadata object. The first step in this method is to check if the `managerKey` exists, if it doesn't, the method will add an additional Operation, to the start of the array, of type `OperationReveal`. This is a required step to perform and will incur another fee for the wallet. This is what most libraries do automatically, always check if the current operation needs a reveal.

Also note that the reveal operation, like all operations, needs a unique numeric `counter` string. Any operation being sent alongside the reveal, must have a higher counter than the reveal operation.





<br/>
<br/>
<br/>

### Step 3: Creating a reveal operation
Similar to the steps in the first tutorial [Sending an Operation](./tutorial-send-operation.md), we need to perform all the requests (Steps are explained more clearly in the the first tutorial):

- Open and send `2_Counter`
- Open and send `3_Head`
- Open and send `4_Forge`,  Note: this request is slightly different to the one in the first tutorial. Open the "Body" tab to see the JSON structure:
  - This request has no `amount` or `destination` like the first operation.
  - Its `kind` property is set to `reveal`.
  - It has a `public_key` property set to the same value you copied into `1_ManagerKey` a moment ago.
- Open and send `5_parse`.
- Go back to the CLI and sign the forged hash
  - Run the sign command, making sure to use the wallet `postman3` this time
    ```bash
	tezos-client sign bytes 0x03<your-forge-hash-here> for postman3
	```
  - Then use Python to convert it to hex:
    ```bash
	python3

	>>> import base58check
	>>> base58check.b58decode(b'<your-signature-here>').hex()
	```
- Open `6_Preapply`, go to the "Pre-request Script", and replace:
  - `<your-signature-here>` with the result of tezos-client sign command
  - `<your-signature-hex-here>` with the result of the python script
  - Click send
- Open and send `7_Inject`

You should have received an opertaionHash again similar to this:

```javascript
"ooDWAUVXSn8kWxUetgNgc5HRKHHfzEh8QcJAZgpeuBCdfkzcPbx"
```

You can check the status of the operation on [TZKT](https://carthage.tzkt.io/), by copying the operationHash into the search bar.










<br/>
<br/>
<br/>

### Step 4: Verifying the reveal
Wait for the operation to go through (again this should take about 1 minute). 

Go back to the first request `1_ManagerKey` in the folder "reveal a public key", and run it again. This time instead of `null`, you should get back the public key that you copied into the "Pre-request Script".

Now your new wallet is setup and ready to send operations, just like the two we got from the faucet.
