# Tutorial: Sending an operation using the RPC

The [Tezos RPC](https://tezos.gitlab.io/api/rpc.html) is a collection of public APIs that developers can use to perform actions on the blockchain (provided you have the right keys), or query data. The RPC accepts and returns JSON. Because the blockchain is decentralised, there is no one place to access the RPC. You either need to host your own node, or use a service provided by someone else. Two examples of free services are [Tezos Giga Node](https://tezos.giganode.io/) and [Nautilus Cloud](https://nautilus.cloud/). We will be using both in the following tutorial.



<br/>

## Introduction

Apart from querying data such as a balance, everything in Tezos involves sending an operation (or "injecting" as it's referred too). If you want to send funds to someone else, inject an operation of type `transaction`. To delegate your funds to a baker, inject an operation of type `delegation` etc. Creating, validating and Injecting an Operation is comprised of 8 steps, requiring 7 API calls in total to complete. Each of these steps has a separate Postman request for you to experiment with, with the exception of signing the payload which needs to be done outside of Postman. These steps and the order they are executed in, will show you everything you need to know to use the RPC in your applications.


<br/>
<br/>
<br/>

## Tutorial

Open the folder "Sending an Operation", choose the "Tezos-Testnet" environment and lets get started.

![Screenshot showing the expanded "Sending an Operation" folder](assets/tutorial-send-begin.png "Sending an Operation")

<br/>

### Step 1: Fetching the manager key
When injecting an operation, you need certain pieces of metadata, one such piece is the current wallet's ManagerKey. We'll need to fetch it from the RPC.

- Click on the GET request `1_ManagerKey` to open the request.
- Click the Send button on the right hand side of the application to fetch the managerKey for the `sourceAddress` you entered into the environment in the getting started instructions.
- You should see the response along the bottom after a brief moment, something like:
  ```javascript
  "edpkuhzNqTkh4nt9yC3cnZUCMqEcrvf6BNF5Gqkn9r5CS6hMyhkYQT"
  ```
- This key will be needed later on, but Postman will take care of this for you. Inside the "Tests" tab, just under the URL, you can see a piece of Javascript is being run to record that key for you and will automatically be used later on.



<br/>
<br/>
<br/>

### Step 2: Fetching the current counter
Every Operation needs a counter. A counter is a numeric string used to indicate the order of operations. Each Operations counter must be 1 higher than the counter of the users previous operation. You will receive an error if you try to send the same counter more than once. Lets get ours from the network.

- Click on the GET request `2_Counter` to open the request.
- Click the Send button to fetch the counter.
- Again, inside the tests tab, this value will be extracted and stored for later use.



<br/>
<br/>
<br/>

### Step 3: Fetching metadata about the blockchain
The last pieces of metadata we need is the current `protocol` being used by the network, the current `chain_id` that we will inject our operation into and the `hash` of the head of the chain. All of these can be found by querying the Head RPC.

- Click on the GET request `3_Head` to open the request.
- Click the Send button to fetch the blockchain HEAD.
- The `protocol`, `chain_id` and `hash` are all top level keys in the response that comes back. Once again the Tests tab will extract and store these for you.


<br/>
<br/>
<br/>

### Step 4: Forge
The blockchain doesn't just accept JSON to inject an operation. The JSON payload needs to be converted or "Forged" into a string of hex bytes, called a hash. Forging is a complicated process, but the RPC provides an API to do it for us.

- Click on the POST request `4_Forge` to open the request.
- First open the "Pre-request Script" tab. First thing that Postman will do is to take the counter from one of the previous calls, and increment it by 1.
- Now open the "Body" tab.
- This is the JSON structure that the Tezos node is going to forge for us. It is an Operation of type transaction, meaning we are going to send XTZ, from the `sourceAddress` you entered in the environment to the `destinationAddress` you entered.
  - The amount is set to `1000000`. XTZ has 6 decimal places, but you can't send decimal places to the RPC. You need to multiply the XTZ you want to send by 1000000 to convert the XTZ (or Tez) into Mutez. Mutez must be a whole number.
  - The `fee`, `gas_limit` and `storage_limit` together form the cost of this transaction. This will be covered in the tutorial to estimate the cost of an operation. You can ignore for now, the fees are set correctly.
  - The updated counter and the hash have already been supplied.
- Now lets click send to see the hex string we get back. Yours will be different, but it should look something like this:
  ```javascript
  "c540cbce653e0a1292a46e982ca3d55d16afb63bab4dc39d0738c5ff4b2b912d6c00ca1c5dd7f6665501b57c692f0726a1db46fd1d18f80afea32684528102c0843d00003e47b9be112a0d86657da58e3b22fa5b29e0fc9d00"
  ```



<br/>
<br/>
<br/>

### Step 5: Parse
Parsing is a security measure. When asking the server to forge for you, you are placing complete trust in it. Without parsing, you are assuming the server hasn't been comprised, or is not malicious, that it hasn't secretly changed the destination address to someone else to steal the funds. Parsing allows us to validate that the forged hex matches what we wanted to send.

... But if the server is comprimised, it would simply lie and return the payload you sent in the first place. So for security reasons, we ask a different server to parse the forge.

- Click on the POST request `5_Parse` to open the request.
- Compare the URL of the forge request to the parse request. We are using two different base URLs. The primary node is Tezos Giga Node, and the parse node is Nautilus Cloud. Using separate servers reduces the risk of a "Blind Signature Attack".
- Postman is performing some pre-processing of the forged hex inside the "Pre-request Script". Before asking the node to parse, we need to strip out the first 32 bytes (64 characters) to remove the block hash. Then we need to add a signature to the end. The Parse API doesn't actually validate the signature is correct, so we can just add a string of 128 zeros to the end, to form an empty signature.
- Then inside "Body" we send the modified forge, and the blockchain head again to the new server.
- Click send and lets examine what we are doing inside the "Tests" tab.
  - First, we grab a copy of the JSON we originally sent to the Forge API.
  - Then we grab the response from the parse API. Which should be an object similar to the one we sent to the Forge API with some minor differences (Parse will return an array, where as we sent an object to Forge. Parse will return the signature, where we didn't send one to the forge).
  - We check that the the parsed branch matches what we sent, and we check that each value of the JSON we sent, matches the value of what was returned.
- Postman will report a Test failure, if the Parse API does not return a response match the forge. This will be displayed in the bottom section like so:

![Screenshot showing the expanded "Sending an Operation" folder](assets/tutorial-send-failed-parse.png "Sending an Operation")

- If it reports a success, we know that we can trust the first server forged the data correctly for us.



<br/>
<br/>
<br/>

### Step 6: Sign
Unfortunately we can't get Postman to complete this step for us. Although we can run code in Postman, and import some small js libraries, we are unable to import the sodium library which provides all the tools we need. We'll first explain what needs to be done, so that you can replicate it in another language if you need to. Then we will walk you through the process of using the CLI to do it so we can finish the tutorial.

- How to sign without the CLI:  

  As mentioned above, the Sodium is a great crypto library to achieve these goals. It comes in many formats for Javascript, swift, kotlin, java etc. Lets look at the steps (a working example can be found in the [camlKit TezosNodeClient](https://gitlab.com/camlcase-dev/camlkit/-/blob/master/Source/Clients/TezosNodeClient.swift) scroll down to the `signPreapplyAndInject(...)` function). 
  
  Steps:
    - You need to take the result of the Forge call, and add a water mark of `03` in hex. All standard operations use the same watermark. Only operations to do with baking and proposals use a different watermark.
	- Then the hex string needs to be converted to an array of Binary bytes (`[UInt8]`).
	- Use the `Sodium.genericHash.hash` function to create a fixed length hash of the input (sodium generic hash uses `Blake2b`).
	- You can then need to sign the bytes with your wallets secretKey. The default signing curve is `ed25519`. (camlKit also supports `secp256k1` if required).
	- This will return a `base58check` of the bytes called the `signature`.
	- Lastly we also need the `signature` converted to binary and encoded in a hex string. Which we will call the "signatureHex"


<br/>
<br/>

- How to sign with the CLI:

  In order to finish the tutorial, we will walk you through how to use the CLI to sign a forge and paste it back into Postman.

  Steps:

    - Run the below command, replace `<your-forged-hex>` with the result of the Forge call from the Postman API. The `0x03` is the digital watermark mentioned above.

	  ```bash
      tezos-client sign bytes 0x03<your-forged-hex> for postman1
      ```
	
      The output should look something like this:
    
	  ```bash
	  Signature: edsigtvkkLfBSznTGyTbhC2sgHBFsDxGohMn3BeuRRSMb4mXDcAz1jfMeCjoVftw4v3v8jySfijdZez6gSopfGC9L1oE19u8fBo
	  ```
	- Open the Postman request `6_Preapply`, open the "Pre-request Script" tab.
	- On the first line replace `<your-signature-here>` with the signature returned from the command above.
	- Now to convert the the signature to it's binary form, the easiest way to do this on mac and linux is to install a python module.
	- Go to your terminal and run this command to install the module (you may need to use `sudo` at the start of the command):
	  ```bash
	  pip3 install base58check
	  ```
	- Now to launch a python terminal, type:
	  ```bash
	  python3
	  ```
	- Import the module:
	  ```
      import base58check
	  ```
	- Use the module to convert the signature, replacing `<signature>` with the signature we created above
	  ```
	  base58check.b58decode(b'<signature>').hex()
	  ```
	- Go back to Postman and the "Pre-request Script" tab, and replace `<your-signature-hex-here>` on the second line with this new value.
	- Important to note, when using the CLI, the signature hex will contain a prefix at the start, and a checksum at the end. These need to be removed before injecting, but it will be done automatically by Postman later. Move on to Step 7 now.





<br/>
<br/>
<br/>

### Step 7: Preapply
Preapply is where we catch potential errors. Up until now all the APIs will only return an error if the server is down, the request is in the wrong format, the data is malformed etc. So long as you send valid JSON, the Forge will always return a forged string. Its only in the Preapply section that we can catch errors such as an invalid counter, invalid destination address etc.

- Click on the POST request `6_Preapply` to open the request.
- We've already filled in the signature in the previous step. We can see how this is used in the "Body" tab. Go ahead and click send.
- The result should look something like this if it was successful:

	```javascript
	[
		{
			"contents": [
				{
					"kind": "transaction",
					"source": "tz1YxUhaLrGr8nbdTGFs2HrJZeUebGMg7cmE",
					"fee": "1400",
					"counter": "2312683",
					"gas_limit": "10500",
					"storage_limit": "257",
					"amount": "1000000",
					"destination": "tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF",
					"metadata": {
						"balance_updates": [
							{
								"kind": "contract",
								"contract": "tz1YxUhaLrGr8nbdTGFs2HrJZeUebGMg7cmE",
								"change": "-1400"
							},
							{
								"kind": "freezer",
								"category": "fees",
								"delegate": "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU",
								"cycle": 330,
								"change": "1400"
							}
						],
						"operation_result": {
							"status": "applied",
							"balance_updates": [
								{
									"kind": "contract",
									"contract": "tz1YxUhaLrGr8nbdTGFs2HrJZeUebGMg7cmE",
									"change": "-1000000"
								},
								{
									"kind": "contract",
									"contract": "tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF",
									"change": "1000000"
								}
							],
							"consumed_gas": "10207"
						}
					}
				}
			],
			"signature": "edsigtahivrSnp9icKQkxt7e6fNiTdYUHLzXp5HLbvrbiNiZNvQUpsooUiEHRJwWYn3kL2FX7tzTTnqrJkdRsYvYiP3qzgt1d6h"
		}
	]
  ```
- or like this if there is an error:
  ```javascript
  [
	  {
		  "kind":"branch",
		  "id":"proto.006-PsCARTHA.contract.counter_in_the_past",
		  "contract":"tz1YxUhaLrGr8nbdTGFs2HrJZeUebGMg7cmE",
		  "expected":"2312684",
		  "found":"2312683"
      }
  ]
  ```
- There are several exceptions to how errors can come down. Sometimes they will appear embedded inside the successful response object. We will discuss this in more detail in a later tutorial. For now Postman will simply check under the "Tests" tab, that we did receive a "contents" object and report a Success or a fail, in the same way the Parse call did.




<br/>
<br/>
<br/>

### Step 8: Inject
The last step is to inject the signed JSON into the blockchain. If successful we will get an opertionHash back that we can use to check the status of the operation.

- Click on the POST request `7_Inject` to open the request.
- The "pre-request Script" tab will automatically remove the prefix and checksum from the signed hex. This may not be necessary in other setups, such as using Sodium.
- The data we need to send is simply a string containing the the result of the Forge call, and the signatureHex we created in the python script previously. Postman will form these together.
- Go ahead and click send, you should see a response like this:
  ```javsacript
  ooyhLFVwcTpXYvRYswdEEV9TyzRuD2XUQb6Dej4pyQbTEUZFpdt
  ```
- Congratulations, your operation is now in the mempool of the Tezos Node and is waiting to be processed by a baker. We can take this operation hash and use a Block explorer, such as [TZKT](https://carthage.tzkt.io/) to check its status. Simply go to the site and paste the hash into the search field. When the status says: "Applied", thats it, the operation is now in a block on the blockchain.
- Rule of thumb in Tezos is that the operation hasn't been "Confirmed" until another 30 - 60 blocks have been added on top of the chain. At this point, the chances of your operation needing to be reversed are incredibly slim. Given a Block is added to the chain every 60 seconds, it will take 30 - 60 minutes to make 100% sure that the operation is there to stay. Chains being reversed is a rare occurrence, but worth noting it is possible to ensure the integrity of the blockchain.





<br/>
<br/>
<br/>

### Conclusion

You have now seen what it takes to send XTZ from one wallet to another using the RPC, following all the best practices using Parse with a separate server, and checking Preapply for errors before proceeding. Its possible to skip these two steps, but they open up your application to security issues and difficult to debug problems. They should always be preformed.

Now that you understand the flow, you can tweak the amounts being sent, the source, destination etc. Just remember you will need to re-run the Counter and Head requests again to refresh the data on the chain.

All operations follow the same process, just with a different JSON payload to be Forged. We will be adding examples of other operations in later versions.

<br/>

Postman has one last trick up its sleeve before we finish up. The point of building this tutorial and using Postman, was to allow other developers more easily translate the steps needed to inject an operation, into their own applications. The tezos-client does a lot behind the scenes and it is difficult to piece it together without reading its source code. By using these Postman requests, you can now convert the steps into a language that will allow you to add it directly to your application, by using the Code Snippet generator.

Open any request, such as the Forge request. and click the code button located underneath the send button

![Screenshot showing the location of the code button](assets/tutorial-send-code-button.png "Code button")

to open the Code Snippet generator:

![Screenshot showing an example of the Code snippet generator](assets/tutorial-send-code-export.png "Code Snippets")

You can select your language and copy paste the snippet directly into your application. You need only supply your own values for values such as the amount, source and destination address etc.


<br/>

Any comments, questions or issues, feel free to open up a gitlab issue on this repo.


